import 'package:flutter/material.dart';
import 'package:flutter_news_bloc/src/blocs/news_bloc.dart';
import 'package:flutter_news_bloc/src/models/latest_news.dart';


class NewsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bloc.fetchAllNews();
    return Scaffold(
      appBar: AppBar(
        title: Text('News List'),
      ),

      //StreamBuilder listen to the incoming strams and update the UI
      //Streambuilder is expecting a stream parameter where we have passing
      // the MoviesBloc allMovies method as it's returning a stram
      body: StreamBuilder(
        stream: bloc.allNews,
        builder: (context, AsyncSnapshot<List<Data>> snapshot) {
          //here snapshot data is holding the ItemModel object
          if (snapshot.hasData) {
            return buildList(snapshot);
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildList(AsyncSnapshot<List<Data>> snapshot) {

    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (context,index){
       /* return new Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.network(snapshot.data.elementAt(index).img_src,width: 150, height: 150.0,),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new Text(snapshot.data.elementAt(index).post_title),
                    ],
                  ),
                ),
              )
            ],
          ),
        );*/

        return ListTile(
          title: Text('${snapshot.data.elementAt(index).post_title}'),
          trailing: Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Image.network('${snapshot.data.elementAt(index).img_src}',width: 150,),
          ),
        );
      },
    );
  }
}
