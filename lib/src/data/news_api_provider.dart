import 'package:flutter_news_bloc/src/models/latest_news.dart';
import 'package:flutter_news_bloc/src/models/serializers.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
//Data Provider
class NewsApiProvider {

  final String baseUrl =
      "https://zoombangla.com/api/index.php?task=selected_news";

  Future<List<Data>> getData() async {
    final response = await http.get((Uri.parse(baseUrl)));

    print(response.body.toString());

    LatestNews data = serializers.deserializeWith(
        LatestNews.serializer, json.decode(response.body));
    return data.items.map((Data data) => data).toList();
  }

}
