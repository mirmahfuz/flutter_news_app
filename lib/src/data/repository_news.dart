import 'dart:async';
import 'package:flutter_news_bloc/src/models/latest_news.dart';

import 'package:flutter_news_bloc/src/data/news_api_provider.dart';

//Data Handler
// RepositoryNews is the central point from where the data will flow to the BLOC.
class RepositoryNews {
  final newsApiProvider = NewsApiProvider();
  Future<List<Data>> fetchAllNews() => newsApiProvider.getData();
}