import 'package:flutter_news_bloc/src/data/repository_news.dart';
import 'package:flutter_news_bloc/src/models/latest_news.dart';
import 'package:rxdart/rxdart.dart';


// Bloc helps in managing state and make access of data from a central
// place in your project.


class NewsBloc {

  //here we have created the repository class object which will
  // be used to access the fetchAllMovies
  final _repository = RepositoryNews();


  //PublishSubject Object whose responsibility is to add the data(got from server)
  // in form of ItemModel object and pass it to the ui screen as stream
  final _newsFetcher = PublishSubject<List<Data>>();


  //And to pass the ItemModel object as stream we have created allNews
  // method  whose return type is Observable
  Observable<List<Data>> get allNews => _newsFetcher.stream;

  fetchAllNews() async {
    List<Data> itemModel = await _repository.fetchAllNews();
    _newsFetcher.sink.add(itemModel);
  }

  dispose() {
    _newsFetcher.close();
  }
}

//Here we have created bloc object
final bloc = NewsBloc();

/*
whenever there is new data coming from server. We have to update the UI
screen. To make this updating task simple we are telling the UI screen
to keep observing any change coming from the MoviesBloc class and
accordingly update your content. This “observing” of new data can be
doing using RxDart.*/
