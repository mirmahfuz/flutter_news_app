import 'package:flutter/material.dart';
import 'package:flutter_news_bloc/src/ui/news_list.dart';


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: NewsList(),
      ),
    );
  }
}